#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
Setup Package Installation
"""


from setuptools import setup, find_packages


__version__ = '0.0.1'


setup(
    name='pydataset',
    version=__version__,
    description='Dataset Toolkit to efficiently process large CSV files',
    author='Anzel Lai',
    author_email='anzel_lai@hotmail.com',
    license='MIT',
    url='https://bitbucket.org/anzellai/',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'cytoolz'
    ]
)

