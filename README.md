# Python Dataset Toolkit

**Python Dataset** is a toolkit to help efficiently process large CSV files in functional approach.


## Requirements

**pydataset** replies on only one external package [**cytoolz**](https://github.com/pytoolz/cytoolz) to achieve functional approach in solving pipeline and data readers.


# Summary and Usage

**pydataset** has implemented several common use cases to achieve MapReduce and Split-Apply-Combine strategy.

Methods in *Pipeline* object are chainable, which will give a pleasant experience similar to [**pandas**](http://pandas.pydata.org/), for example:

```
from pydataset import Dataset, Pipeline

# Dataset accepts both string of system path or file object
my_reader = Dataset(
    '/path/to/my/csv_file.csv',
    header=False,
    fields=['col1', 'col2', 'col3']
)

# You can construct a Pipeline and then start calling chainable methods.
(
    Pipieline('pipeline name')
    .prepare(my_reader.parse())
    .map_fields(['col1', 'col3'])
    .map(lambda row: {row['col1']: row['col3'])
    .merge(sum)
    .consume()
    .unpack()
    .show()
)
```


## Run Tests

There is a sample *main.py* created demostrating how to use this library for a task using external CSV dataset from NHS, and calculated the required results.
Data is not included in this repository.


```
python3 run_tests.py
```

or running unittest directly:

```
python3 -m unittest pydataset/tests/*
```