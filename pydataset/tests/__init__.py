#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
Unittest for Dataset Toolkit
"""

from decimal import Decimal
import inspect
from io import StringIO
import unittest

from ..core import Dataset, Pipeline, Typings


__all__ = ['DatasetTestCase', 'PipelineTestCase']


class DatasetTestCase(unittest.TestCase):
    """Dataset TestCase"""

    def setUp(self):
        self.source = StringIO(
            'col 1,col 2,col 3,col 4,col 5\n'
            'r1c1,r1c2,1.1,1,1\n'
            'r2c1,r2c2,2.2,2,2\n'
            'r3c1,r3c2,3.3,3,3\n'
        )
        self.reader = Dataset(
            self.source,
            header=True,
            typings=[
                Typings.String, Typings.String, Typings.Float,
                Typings.Int, Typings.Int,
            ]
        )

    def test_dataset_can_read(self):
        """Test Dataset can read and parse with correct types."""
        output = list(self.reader.parse())
        exp_fields = ['col_1', 'col_2', 'col_3', 'col_4', 'col_5']
        self.assertEqual(len(output), 3)
        self.assertEqual(self.reader.fields, exp_fields)
        self.assertTrue(isinstance(output[0]['col_1'], str))
        self.assertTrue(isinstance(output[0]['col_3'], Decimal))
        self.assertTrue(isinstance(output[0]['col_4'], int))

    def test_dataset_can_filter(self):
        """Test Dataset can filter and parse with correct types."""
        output = list(self.reader.filter(key=lambda x: x['col_1'] == 'r2c1'))
        self.assertEqual(len(output), 1)
        self.assertEqual(output[0]['col_1'], 'r2c1')
        self.assertTrue(isinstance(output[0]['col_2'], str))
        self.assertTrue(isinstance(output[0]['col_3'], Decimal))
        self.assertTrue(isinstance(output[0]['col_4'], int))

    def test_dataset_without_typings(self):
        """Test Dataset without typings will render all as String."""
        self.reader = Dataset(
            self.source,
            header=True,
        )
        output = list(self.reader.parse())
        self.assertEqual(len(output), 3)
        self.assertTrue(isinstance(output[0]['col_3'], str))
        self.assertTrue(isinstance(output[0]['col_4'], str))

    def test_dataset_will_yield_nothing(self):
        """Test Dataset will filter but yield None without match."""
        output = list(self.reader.filter(key=lambda x: x['col_1'] == 'hello'))
        self.assertEqual(len(output), 0)


class PipelineTestCase(unittest.TestCase):
    """Pipeline TestCase"""

    def setUp(self):
        self.source = StringIO(
            'col 1,col 2,col 3,col 4,col 5\n'
            'r1c1,r1c2,1.1,1,1\n'
            'r2c1,r2c2,2.2,2,2\n'
            'r3c1,r3c2,3.3,3,3\n'
        )
        self.reader = Dataset(
            self.source,
            header=True,
            typings=[
                Typings.String, Typings.String, Typings.Float,
                Typings.Int, Typings.Int,
            ]
        )
        self.pipeline = (
            Pipeline('Pipeline TestCase - ')
            .prepare(self.reader.parse())
        )

    def test_pipeline_can_render_reader(self):
        """Test Pipeline can render reader correctly."""
        self.pipeline.name += inspect.currentframe().f_code.co_name
        output = (
            self.pipeline
            .consume()
            .unpack()
            .show()
            .reader
        )
        self.assertEqual(len(output), 3)
        self.assertTrue(isinstance(output[0]['col_1'], str))
        self.assertTrue(isinstance(output[0]['col_3'], Decimal))
        self.assertTrue(isinstance(output[0]['col_4'], int))

    def test_pipeline_can_pipe_nothing(self):
        """Test Pipeline can still pipe on reader without match."""
        self.pipeline.name += inspect.currentframe().f_code.co_name
        output = (
            self.pipeline
            .prepare(self.reader.filter(key=lambda x: x['col_1'] == 'hello'))
            .map(lambda row: {row['col_1']: row['col_2']})
            .consume()
            .unpack()
            .show()
        )
        self.assertEqual(len(output.reader), 0)

    def test_pipeline_can_pipe_map(self):
        """Test Pipeline can process map pipe."""
        self.pipeline.name += inspect.currentframe().f_code.co_name
        output = (
            self.pipeline
            .map(lambda x: {'col_1': x['col_1']})
            .consume()
            .unpack()
            .show()
        )
        self.assertEqual(output.reader[0]['col_1'], 'r1c1')
        with self.assertRaises(KeyError):
            _ = output.reader[0]['col_2']

    def test_pipeline_can_pipe_merge(self):
        """Test Pipeline can process merge pipe."""
        self.pipeline.name += inspect.currentframe().f_code.co_name
        output = (
            self.pipeline
            .map(lambda x: {'col_5': x['col_5']})
            .merge(sum)
            .consume()
            .unpack(dict)
            .show()
        )
        self.assertEqual(output.reader['col_5'], 6)

    def test_pipeline_can_pipe_join(self):
        """Test Pipeline can process join pipe and reuse result to compute."""
        self.pipeline.name += inspect.currentframe().f_code.co_name
        source_join = StringIO(
            'xxx 1,xxx 2,xxx 3,xxx 4,xxx 5\n'
            'r1c1,hello,3.3,3,3\n'
            'r2c1,world,2.2,2,2\n'
            'r3c1,hello,1.1,1,1\n'
        )
        reader_join = Dataset(
            source_join,
            header=True,
            typings=[
                Typings.String, Typings.String, Typings.Float,
                Typings.Int, Typings.Int,
            ]
        )
        output = (
            self.pipeline
            .prepare(self.reader.parse())
            .join(
                left_reader=reader_join.parse(),
                left_on='xxx_1',
                left_with=['xxx_2', 'xxx_5'],
                right_on='col_1',
                right_with=['col_2', 'col_5'],
            )
            .map(
                lambda row: {
                    (row['xxx_2'] or row['col_2'] or 'unknown'): row['xxx_5']
                }
            )
            .merge(sum)
            .sorted(key=lambda x: x[1], reverse=True)
            .consume()
            .take(1)
            .unpack(list)
            .show()
        )
        self.assertEqual(len(output.reader), 1)
        self.assertEqual(output.reader[0][0], 'hello')
        self.assertEqual(output.reader[0][1], 4)



if __name__ == '__main__':
    unittest.main()
