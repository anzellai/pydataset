#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
Dataset Toolkit core module
"""

import csv
from contextlib import ContextDecorator
from decimal import Decimal
import functools
import itertools
import sys
import threading
import time

try:
    import cytoolz.curried as F
except ImportError:
    sys.stdout.write(
        '\n"cytoolz" cannot be found, please install via:\n'
        '>>> pip install cytoolz\n'
    )
    raise


class Pipeline(object):
    """MapReduce Pipeline with functional approach."""
    pipelines = []
    sort = False
    sort_key = None
    sort_dir = False
    reader = None

    def __new__(cls, name, reader=None):
        # type: (str) -> Pipeline
        cls.name = name
        cls.reader = reader
        return super(Pipeline, cls).__new__(cls)

    def prepare(self, reader):
        # type: (Iterable) -> Pipeline
        """Setup reader in Pipeline"""
        self.reader = reader
        return self

    def add(self, pipeline):
        # type: (Tuple[str, *Any]) -> Pipeline
        """Add a pipeline to the runner."""
        self.pipelines = [*self.pipelines, pipeline]
        return self

    def count(self):
        # type: (None) -> Pipeline
        """Count reader rows"""
        return self.add(F.count)

    def filter(self, func):
        # type: (Callable], Any) -> Pipeline
        """Filter reader row by provided function."""
        return self.add(F.filter(func))

    def merge(self, func):
        # type: (Callable) -> Pipeline
        """Merge each row item with provided function"""
        return self.add(F.merge_with(func))

    def join(self, left_reader=None, left_on='', left_with=None,
             right_reader=None, right_on='', right_with=None):
        # type: (Dataset, str, List[str], Dataset, str, List[str]) -> Pipeline
        """Join another Dataset by field and set up newly joined reader."""
        left_join = [left_on]
        if left_with:
            left_join.extend(left_with)
        right_join = [right_on]
        if right_with:
            right_join.extend(right_with)
        if not right_reader:
            right_reader = self.reader

        self.reader = F.join(
            F.first, F.map(F.get(left_join), left_reader),
            F.first, F.map(F.get(right_join), right_reader),
        )
        self.pipelines = [
            F.map(
                lambda row: {
                    **{left: row[0][idx] for idx, left in enumerate(left_join)},
                    **{right: row[1][idx] for idx, right in enumerate(right_join)},
                }
            )
        ]
        return self

    def map_fields(self, fields):
        # type: (List[str]) -> Pipeline
        """Map returned fields of each row in reader."""
        return self.map(
            lambda row: {
                field: row.get(field) for field in fields
            }
        )

    def average_field_values(self, field, divided_by):
        # type: (str, str) -> Pipeline
        """Calculate the average of values from provided fields."""
        return self.add(lambda row: row[field] / row[divided_by])

    def map(self, func):
        # type: (Callable) -> Pipeline
        """Map each row in reader by provided function."""
        return self.add(F.map(func))

    def reduceby(self, key, func):
        # type: (Union[str, Callable], Callable) -> Pipeline
        """Group by key/function and set up the new MapReduce reader."""
        return self.add(F.reduceby(key, func))

    def unpack(self, type_=None):
        # type: (Union[None, str, Iterable]) -> Pipeline
        """Unpack reader map/generator result as provided type or method."""
        if type_ is None:
            result = []
            for row in self.reader:
                result.append(row)
            self.reader = result
            self.pipelines = []
            return self
        if isinstance(type_, str):
            self.reader = getattr(self.reader, type_)()
            self.pipelines = []
            return self
        return self.add(type_)

    def sorted(self, key=None, reverse=False):
        # type: (Union[None, Callable], bool) -> Pipeline
        """Set up pipeline to sort reader with provided function."""
        self.sort = True
        self.sort_key = key or F.get(0)
        self.sort_dir = reverse
        return self

    def show(self):
        # type: (None) -> Pipeline
        """Show reader result in system stdout."""
        if hasattr(self.reader, '__iter__'):
            sys.stdout.write('\nResult:\n')
            for row in self.reader:
                sys.stdout.write('{}\n'.format(row))
        else:
            sys.stdout.write('\nResult: {}\n'.format(self.reader))
        return self

    def take(self, item):
        # type: (Union[int, str]) -> Pipeline
        """
        Slice reader by item as key.

        If reader is tuple type, return 0-indexed item
        >>> Pipeline.reader = (1, 2, 3)
        >>> Pipeline.take(1)
        2

        If reader is list type, return upto 0-indexed items
        >>> Pipeline.reader = [1, 2, 3]
        >>> Pipeline.take(2)
        [1, 2]

        If reader is dict type, return item by key
        >>> Pipeline.reader = {'a': 1, 'b': 2, 'c': 3}
        >>> Pipeline.take('b')
        2
        """
        if isinstance(self.reader, tuple):
            self.reader = self.reader[item]
        elif isinstance(self.reader, list):
            self.reader = self.reader[:item]
        elif isinstance(self.reader, dict):
            self.reader = {item: self.reader[item]}
        return self

    def consume(self, reader=None):
        # type: (None, Dataset) -> Any
        """Consume pipelines and set result as new reader."""
        with Timeit(self.name):
            self.reader = F.pipe(
                reader or self.reader,
                *self.pipelines,
            )
            if callable(self.reader):
                self.reader = self.reader()
            if self.sort:
                if hasattr(self.reader, 'items'):
                    self.reader = self.reader.items()
                result = F.sorted(
                    self.reader,
                    key=self.sort_key,
                    reverse=self.sort_dir
                )
                self.reader = result
            self.pipelines = []
            return self


# NOTE: should extend handling of CSV seperator, quotechar etc.
class Dataset(object):
    """Dataset object to hold CSV reader for use in Pipeline."""
    def __init__(self, path, header=True, fields=None, typings=None):
        # type: (str, bool, List[str], List[Typings]) -> None
        self.path = path
        self.header = header
        self.fields = fields
        self.typings = typings
        self.reader = None

    def write_header(self, output=None, fields=None):
        # type: (str, List[str]) -> None
        """Write dataset headers to output file."""
        with open(output or self.path, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=fields or self.fields)
            writer.writeheader()

    def write_row(self, row, output=None, fields=None):
        # type: (dict, str, List[str]) -> None
        """Write dict row to output file"""
        with open(output or self.path, 'a') as f:
            writer = csv.DictWriter(f, fieldnames=fields or self.fields)
            row = {
                col: val for col, val in row.items()
                if col in writer.fieldnames
            }
            writer.writerow(row)

    def filter(self, key=None):
        # type: (Callable) -> Generator[None, dict]
        """Filter reader by provided function."""
        for row in self.parse():
            if key(row):
                yield row

    def parse_row(self, row):
        # type: (dict) -> dict
        """Return parsed row with matching Typings method."""
        if not self.fields:
            raise TypeError('Data fields cannot be empty!')

        row = list(row)
        if not self.typings:
            self.typings = [Typings.String] * len(self.fields)
        for index, method in enumerate(self.typings):
            row[index] = method(row[index])

        return dict(zip(self.fields, row))

    def parse(self):
        # type: (None) -> Generator[dict]
        """Yield each row in file reader after Typings method is parsed."""
        data = self.path
        if str(self.path) == self.path:
            data = open(self.path, 'r')
        with data:
            if self.header:
                self.fields = [
                    col.strip().replace(' ', '_')
                    for col in data.readline().split(',')
                ]
            reader = csv.reader(data)
            for row in map(self.parse_row, reader):
                yield row


class Typings(object):
    """Typings provide parsing method for generic Python objects."""
    @classmethod
    @functools.lru_cache(maxsize=9999, typed=True)
    def String(cls, value):
        # type: (Any) -> str
        """Convert value to string."""
        try:
            return value.strip() if value else ''
        except (ValueError, TypeError):
            return ''

    @classmethod
    @functools.lru_cache(maxsize=9999, typed=True)
    def Int(cls, value):
        # type: (Any) -> int
        """Convert value to integer."""
        try:
            return int(cls.String(value))
        except (ValueError, TypeError):
            return 0

    @classmethod
    @functools.lru_cache(maxsize=9999, typed=True)
    def Float(cls, value):
        # type: (Any) -> float
        """Convert value to float (Python Decimal for precision)."""
        try:
            return Decimal(cls.String(value))
        except (ValueError, TypeError):
            return Decimal(0)


class Timeit(ContextDecorator):
    """
    A context manager to wrap a code block and show duration of running
    """
    desc = ''
    anchor = None
    spinner = None
    stdout = sys.stdout

    def __init__(self, desc='', stdout=None):
        # type: (str, Any) -> None
        self.desc = desc
        self.spinner = itertools.cycle(['|', '/', '-', '\\'])
        if stdout:
            self.stdout = stdout

    def spin(self):
        # type: (None) -> None
        """Show the spinner in a separate thread"""
        self.stdout.write('\033[95mStarted \033[0m')
        while True:
            msg = '\033[95m{}\033[0m'.format(next(self.spinner))
            self.stdout.write(msg)
            self.stdout.flush()
            time.sleep(0.4)
            self.stdout.write('\b')

    def start(self):
        # type: (None) -> None
        """Start the thread to pipe message in stdout"""
        if not self.stdout.isatty():
            return
        console = threading.Thread(target=self.spin)
        console.daemon = True
        console.start()

    def __enter__(self):
        # type: (None) -> None
        longest = max(map(len, self.desc.split('\n')))
        print('\n{}\n'.format('-' * longest))
        print(self.desc)
        print('\n{}\n'.format('=' * longest))
        self.anchor = time.time()
        self.start()
        return self

    def __exit__(self, *exc):
        # type: (None) -> None
        self.stdout.write('\r')
        now = time.time()
        msg = 'Completed in {:.5f}s'.format(now - self.anchor)
        if self.stdout.isatty():
            msg = '\033[95m' + msg + '\033[0m'
        self.stdout.write(msg + '\n')
        self.stdout.flush()
        return False
