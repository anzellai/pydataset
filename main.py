#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
Main Runner to compute given question sets
"""

from pydataset import Dataset, Pipeline, Typings


def main():
    """Main entry point of this runner"""
    # Reader REXT without header
    # We should provide the header names
    # Without Typings field values will cast to Typings.String
    reader_rext = Dataset(
        'T201202ADD REXT.CSV',
        header=False,
        fields=[
            'PERIOD', 'CODE', 'NAME', 'LINE1', 'LINE2', 'CITY', 'COUNTY',
            'POSTCODE',
        ]
    )
    # Reader IEXT sample row (
    #   SHA='Q39', PCT='5ZW', PRACTICE='L83679', BNF_CODE='1201010F0',
    #   BNF_NAME='Flumetasone Pivalate', ITEMS=7, NIC=13.5, ACT_COST=12.67,
    #   PERIOD='201109',
    # )
    # With Typings, we can cast the field values to Python types
    reader_iext = Dataset(
        'T201109PDP IEXT.CSV',
        typings=[
            Typings.String, Typings.String, Typings.String, Typings.String,
            Typings.String, Typings.Int, Typings.Float, Typings.Float,
            Typings.String,
        ]
    )

    # Answer 1
    answer1 = (
        Pipeline('1. How many practices are in London?')
        .prepare(
            reader_rext.filter(
                key=lambda row: (
                    row['CITY'] == 'LONDON' or row['COUNTY'] == 'LONDON'
                )
            )
        )
        .count()
        .consume()
        .show()
    )

    # Answer 2
    answer2 = (
        Pipeline('2. What was the average actual cost '
                 'of all peppermint oil prescriptions?')
        .prepare(reader_iext.parse())
        .filter(
            lambda row: row['BNF_NAME'] == 'Peppermint Oil'
        )
        .map_fields(['ACT_COST', 'ITEMS'])
        .merge(sum)
        .average_field_values('ACT_COST', 'ITEMS')
        .consume()
        .show()
    )

    # Answer 3
    answer3 = (
        Pipeline('3. Which 5 post codes have the highest actual spend, '
                 'and how much did each spend in total?')
        .join(left_reader=reader_rext.parse(),
              left_on='CODE',
              left_with=['POSTCODE'],
              right_reader=reader_iext.parse(),
              right_on='PRACTICE',
              right_with=['NIC'])
        .map(lambda row: {row['POSTCODE']: row['NIC']})
        .merge(sum)
        .sorted(key=lambda x: x[1], reverse=True)
        .consume()
        .take(5)
        .show()
    )

    # Answer 4
    # NOTE: Here is a helper function to perform mutation on dict
    def merge_add(seq, item):
        """Helper function to sum items within dict"""
        seq['NIC'] += item['NIC']
        seq['ITEMS'] += item['ITEMS']
        return seq

    # Answer 4a reader can be reused by Answer 4b
    answer4a = (
        Pipeline('4. For each region of England '
                 '(North East, South West, London, etc.\n'
                 '  a. What was the average price per prescription of '
                 'Flucloxacillin (excluding Co- Fluampicil)?')
        .join(
            left_reader=reader_rext.parse(),
            left_on='CODE',
            left_with=['CITY', 'COUNTY'],
            right_reader=reader_iext.filter(
                lambda row: (
                    'Flucloxacillin' in row['BNF_NAME'] and
                    'Co- Fluampicil' not in row['BNF_NAME']
                )
            ),
            right_on='PRACTICE',
            right_with=['NIC', 'ITEMS']
        )
        .map(
            lambda row: {
                'REGION': row['COUNTY'] or row['CITY'] or 'UNKNOWN',
                'NIC': row['NIC'],
                'ITEMS': row['ITEMS']
            }
        )
        .reduceby('REGION', merge_add)
        .unpack(lambda row: dict(row).values())
        .map_fields(['REGION', 'NIC', 'ITEMS'])
        .map(
            lambda row: {
                'REGION': row['REGION'],
                'NIC': row['NIC'],
                'ITEMS': row['ITEMS'],
                'AVERAGE': row['NIC'] / row['ITEMS'],
            }
        )
        .consume()
        .unpack()
        .show()
    )

    # Calculate national mean for use in Answer 4b
    national_mean = (
        Pipeline('4. For each region of England '
                 '(North East, South West, London, etc.\n'
                 '  National mean is:')
        .prepare(answer4a.reader)
        .map_fields(['NIC', 'ITEMS'])
        .merge(sum)
        .average_field_values('NIC', 'ITEMS')
        .consume()
        .show()
    )

    # Answer 4b relies on reader from Answer 4a & national_mean
    answer4b = (
        Pipeline('4. For each region of England '
                 '(North East, South West, London, etc.\n'
                 '  b. How much did this vary from the national mean?')
        .prepare(answer4a.reader)
        .unpack()
        .map(
            lambda row: {
                'REGION': row['REGION'],
                'AVERAGE': row['AVERAGE'],
                'VARIANCE': row['AVERAGE'] - national_mean.reader
            }
        )
        .consume()
        .unpack()
        .show()
    )

    # Answer 5
    answer5 = (
        Pipeline('5. Come up with your own interesting question about NHS '
                 'prescriptions in England \n'
                 'and use the data (plus any other sources you\'d like to use) '
                 'to answer it.\n'
                 'Which prescription has the highest quantity sold?')
        .prepare(reader_iext.parse())
        .map_fields(['BNF_CODE', 'ITEMS'])
        .map(lambda row: {row['BNF_CODE']: row['ITEMS']})
        .merge(sum)
        .sorted(key=lambda x: x[1], reverse=True)
        .consume()
        .unpack()
        .take(1)
        .show()
    )

if __name__ == '__main__':
    # should really use multiprocessing to speed up by Parallelism
    main()
